---
title: '22S PBa IT sikkerhed'
subtitle: 'Fagplan for Netværks- og kommunikationssikkerhed'
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mbn@mnworks.dk'
left-header: \today
right-header: 'Fagplan for Netværks- og kommunikationssikkerhed'
skip-toc: true
skip-lof: true
filename: 22S-ITS-NWSEC_lecture_plan
semester: 22S
---

# Introduktion

I dette fag vil teknologier og metoder bliver præsenteret, og der vil blive arbejdet med det.

Det er tænkt som introduktion og name-dropping, og den studerende skal således selv supplere op for at få maksimal udbytte.

Faget er på 10 ECTS point.

# Læringsmål

Viden
Den studerende har viden om og forståelse for:

* Netværkstrusler
* Trådløs sikkerhed
* Sikkerhed i TCP/IP
* Adressering i de forskellige lag
* Dybdegående kendskab til flere af de mest anvendte internet protokoller (ssl)
* Hvilke enheder, der anvender hvilke protokoller
* Forskellige sniffing strategier og teknikker
* Netværk management (overvågning/logning, snmp)
* Forskellige VPN setups
* Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI).

Færdigheder
Den studerende kan:

* Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)
* Teste netværk for angreb rettet mod de mest anvendte protokoller
* Identificere sårbarheder som et netværk kan have.

Kompetencer
Den studerende kan håndtere udviklingsorienterede situationer herunder:

* Designe, konstruere og implementere samt teste et sikkert netværk
* Monitorere og administrere et netværks komponenter
* Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report)
* Opsætte og konfigurere et IDS eller IPS.

Se [Studieordningen sektion 2.4](https://www.ucl.dk/link/3a5f054541da4bbca44c677abf1ee8c3.aspx?nocache=77112065-b434-4e74-a5b8-336327053716)

# Lektionsplan

| Teacher | Week | Aktivitet |  
| :--: | :---:  | :--- |
| MON | 06 | Introduktion til faget. Tooling up: VMs og linux.|
| MON | 07 | Del 1: Forstå netværk - OSI modellen og network hardware: routere, switch, AP, 802.3, 802.11, firewalls |  
| MON | 08 | Netværkstopologi: subnets, IP adresser, VLANs |
| MON | 09 | Bygge netværk: Virtuelle netværk, fysiske netværk |
| MON | 10 | Netværks protokoller: Wireshark, sniffe trafik, HTTP, HTTPS, m. fl. |
| MON | 11 | Monitorering: SNMP, syslog, Prometheus, Grafana |
| MON | 12 | Network services og applikationer: DNS, Webapps, APIer. |  
| MON | 13 | Del 2: Sikkerhed på netværk - OSI modellen igen |  
| MON | 14 | Hardware sikkerhed: wireless sniffing, 802.1x, firewall rules |  
| MON | 15 | Påske |
| MON | 16 | Påske |
| MON | 17 | Crowdsec workshop |
| MON | 18 | Hardware sikkerhed lag 5+: application layer security. NG firewalls. Certificates |
| MON | 19 | Topologier: segmentation, DMZ, non-routable networks, lateral movement |
| MON | 20 | Network monitoring: sniffing, DNS/NS, scanning, shodahq, nmap |
| MON | 21 | Eksamens recap, og emne gennemgang |
| MON | 23 | Eksmener |

