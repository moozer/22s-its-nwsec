---
title: 'ITS NW security'
subtitle: 'Exercises'
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'ITS NW security, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans.

References

* [Weekly plans](https://moozer.gitlab.io/22s-its-nwsec/22S_ITS_nwsec_weekly_plans.pdf)
