---
Week: 07
tags:
- ISO model
- Networking
- Hardware
---

\pagebreak

# Opgaver for ww07

Networking hardware

## Opgave 1 - specs

Vi vil se på specs for forskelligt hardware, og sammenligne.

### Opgave beskrivelse

1. Find listen fra ww06, hvor lavede vi en liste over hardware og deres plads i OSI modellen.
2. Udvælg 2-3 forskellige enheder fra listen
2. Find specifikationerne for enheden
4. Hvad bliver listet som begrænsninger? Synes du det er høje tal? lave tal?

### Links

Som eksempel, [datablad for Junipe SRXes](https://www.juniper.net/us/en/products/security/srx-series/srx300-line-services-gateways-branch-datasheet.html)


## Opgave 2 - internet speeds

Der er flere services på nettet der tilbyder at teste båndbredde

### Opgave beskrivelse

1. Gå på [speedtest](https://speedtest.net)
2. Hvad er hastigheden?
3. Best guess: hvad er bottleneck, og hvorfor.

Prøv evt. at køre den samtidigt på to maskiner på samme subnet/wifi.

### Links

None Supplied

## Opgave 3 - more internet speed

`Iperf` er et værktøj til den slags.

### Opgave beskrivelse

1. Installér iperf
2. Kør Iperf mod en public server, e.g. `iperf3 -c <some server> -t 10 -i 1 -p <some port>`
3. Kør Iperf mod en public server (modsat retning), e.g. `iperf3 -c <some server> -t 10 -i 1 -p <some port> -R`
4. Sammenlign resultat. Var hastigheder som forventet?

Note: Der er skrappe begrænsninger på brugen af de forskellige servere, f.eks at der kun er en ad gangen der kan bruge servicen.

### Links

Iperf relaterede links:

* [iperf commands](https://iperf.fr/iperf-servers.php)
* [public iperf server](https://fasterdata.es.net/performance-testing/network-troubleshooting-tools/iperf/)

## Optional: Opgave 3b - din egen iperf server

iperf er et open source client/server system, så man kan sætte sin egen server og lave sine egne tests.

### Opgave beskrivelse

1. Installér iperf på en klient og på en server.
2. Iperf fra klient til server.
3. Matcher det din forventning til hastigheden.

### Links

None supplied