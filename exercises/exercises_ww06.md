---
Week: 06
tags:
- ISO model
- Networking
- Hardware
---

\pagebreak

# Opgaver for ww06

Intro til faget

## Opgave 1 - OSI model

OSI model har 7 lag.

### Opgave beskrivelse

1. Find almindelige (consumer) eksempler på hardware enheder, og placer dem på OSI modellen.
2. Find nye/sjældne/esoteriske enheder (fysiske eller virtuelle) (med referencer) - f.eks. next-gen firewalls, proxies, application gateways.
   Hvad-som-helst med et netstik/wifi er ok at have med.

### Links

None Supplied

## Opgave 2 - Troubleshooting

Dette er en øvelse i fejlfinding, og for at få testet tankegangen når man arbejder struktureret.

Tag et problem I har i løbet af ugen, og gennemarbejd det.

### Opgave beskrivelse

1. Vælg et problem, gerne når det opstår, alternativt på bagkant.

   * Det kunne være printerfejl, wifi connection issues, hjemmesider der er 'væk', langsomt internet, certifikat fejl, mv

2. Beskriv problemet. Vær opmærksom på forskellen mellem hvad der er observeret, og hvad der er "rygmarvsreaktion".
3. Beskriv hvad du tror problemet er (ie. formuler en hypotese)
4. Find oplysninger der underbygger/afkræfter ideen (ie. se i logfiler, lav tests, check lysdioder, ...) og skriv det ned
5. Hvis ideen blev afkræftet, lav en ny hypotese.
6. Beslut hvad der kan gøres for at afhjælpe eller mitigere fejlen, hvis den skulle komme igen. 
7. Implementer det hvis ressourcerne tillader det, eller beskrev hvordan det skal håndteres til næste gang.

Bonus spørgsmål: Hvordan håndterede du at nå grænsen for din viden og/eller forståelse af problemet eller et del-element?

### Links

None supplied