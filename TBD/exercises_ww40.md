---
Week: 40
tags:
- Networking
- Web server
- Wireshark
- HTTP
- DNS
- Nginx
- Reverse proxy
- Dig
---


\pagebreak

# Opgaver for ww40

Implementering af netværk

## Opgave 1 - Basic HTTP

I har allerede en webserver

### Opgavebeskrivelse

1. Skim RFC'en: [RFC2616](https://tools.ietf.org/html/rfc2616)

2. Start wireshark og connect til webserveren vha. en almindelig browser fra host og fra kali

   Genfind trafikken, gem pcaps

2. Start wireshark og connect til webserveren vha. en ualmindelig browser (e.g. w3m eller telnet) fra kali

   Genfind trafikken, gem pcaps

4. Sammelign. Er der forskelle?

### Links

Ingen

## Opgave 2 - multiple hosts

Webservere har mulighed for at have flere virtual hosts.

I Nginx hedder det "server blocks"

### Opgavebeskrivelse

1. Se på eksemplerne fra [nginx dokumentationen](https://www.nginx.com/resources/wiki/start/topics/examples/server_blocks/)
2. Beslut et design, hvor man kan teste multiple server block inklusiv en default, og gerne "wildcard" løsningen
3. Beslut hvordan det testes (inklusiv at bruge wireshark)
4. Implementer og test


### Links

Ingen

## Opgave 3 - DNS and RFC


### Opgavebeskrivelse

1. Skim RFC'erne om DNS. Det er primært [rfc1034](https://tools.ietf.org/html/rfc1034), [rfc1035](https://tools.ietf.org/html/rfc1035), [rfc1183](https://tools.ietf.org/html/rfc1183) og [bcp219](https://tools.ietf.org/html/bcp219)

   Hav fokus på RRs sammenlign med [wikipedia](https://en.wikipedia.org/wiki/List_of_DNS_record_types).

2. Brug `dig` til at undersøge `ucl.dk`

   Hvad er der af RR'ere? Hvad betyder de?


### Links

Ingen
