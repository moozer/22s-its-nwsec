---
Week: 48
tags:
- Networking
- MITRE
- Lateral movement
---

\pagebreak

# Opgaver for ww48

## Exercise 1 - Latertal movement techniques

MITRE har en liste, som vi tage udgangspunkt i.

## Opgave beskrivelse

1. Kildekritik: Hvem er MITRE?
1. Se på [MITREs lateral movemnet tactics](https://attack.mitre.org/tactics/TA0008/) og forstå hvad det omhandler.
3. Der er 20 taktikker. Se på 2-3 af dem.

    Enkeltvist eller i par, koordiner hvilke i ser på, så vi dækker bredt.

4. PP på tavle, og plenum diskussion.

### Links

None supplied


## Exercise 2 - juniperlab revisited (again)

Vi vil se på det originale og det opdaterede diagram for vore setup i juniper lab.

## Opgave beskrivelse

1. Find det original diagram og det fra uge 46.

2. Antag at en af enheder er blevet kompromiteret.

    Koordiner mellem grupperne, så vi dækker alle enheder.

3. Angriber scenarier

    * Hvilke lateral movement muligheder har en angriber?
    * har det ændret noget at vi har opdateret netværkslyout?
    * hvordan detekterer vi at der er et problem?

4. Gentag 2+3

5. Præsentation og diskussion på klassen

### Links

None supplied
