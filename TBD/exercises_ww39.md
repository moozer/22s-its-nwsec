---
Week: 39
tags:
- Networking
- VM
- OSPF
- SSH
- Web server
---

\pagebreak

# Opgaver for ww39

Implementering af netværk

## Opgave 1 - Networking

Vi vil lave et multisubnet netværk - uden NAT - hvor alle kan tilgå en bagvedliggende web server.

Dette er en Joint effort opgave, hvor vi laver den sammen på tavlen og på PC'erne.

### Opgave beskrivelse

0. Lave repo på gitlab

Iteration 1 - Et subnet per VM

1. Designe netværk med tilhørende tests. Hvordan får vi internet adgang?
   Docs på gitlab
2. Installere en minimal debian og test at den virker.
   Docs på gitlab om hvordan man cloner
2. Klon debian maskinen og installer en webserver.
   Test at det virker.
3. Installere en openbsd router
4. Kombinere de enkelte VM netværk via en fysisk switch
   Hvad virker?
5. Konfigurer OSPF
3. Test at det virker (og lav et script der nemt tester alle relevante maskiner)

Iteration 2 - Tilføj et MGNT subnet

Vi skal have et fælles MGNT netværk hvor alle routerne er tilgængelige via SSH.

1. Design netværk inkl. IP adresser og VLAN ids
2. Konfigurer routere
3. Test


### Links

Noteark er [her](https://pad.tyk.nu/p/190920_loadbalancer)
