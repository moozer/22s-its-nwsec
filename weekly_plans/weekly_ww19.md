---
Week: 19
Content: Topologier, segmentering, lateral movement
Material: See links in weekly plan
Initials: MON
---

# Uge 19

I denne uge vil vi snakke om hvordan angribere arbejder, når de kompromiterer et netværk, og hvordan vi kan gøre det sværere for dem vha. segmentering.

## Mål for ugen

Practical and learning goals for the period is as follows

### Praktiske mål

Ingen

### Læringsmål

* Den studerende kan forklare og bruge netværkssegmentering i design fasen
* Den studerende kan forklare almindelige sikkerhedstiltag på netværksniveau (L1-L4)
* Den studerende kan forklare brugen MITRE att&ck framework

Se også uge 8

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
|  8:15 | Lektier: PKI and web server |
|  9:15 | The cyber kill chain |
| 10:00 | Lateral movement |
| 11:15 | Frokost  |
| 12:00 | MITRE att&ck. |
| 13:15 | lektier til næste gang |
| 13:30 | the end |

## Kommentarer

* Cyber kill chain
    * jeg har altid fundet den som værende meget militært inspireret
    * by [csoonline](https://www.csoonline.com/article/2134037/what-is-the-cyber-kill-chain-a-model-for-tracing-cyberattacks.html)
    * by [computer.org](https://www.computer.org/publications/tech-news/trends/what-is-the-cyber-kill-chain-and-how-it-can-protect-against-attacks/)
    * video by [the ciso perspective](https://www.youtube.com/watch?v=II91fiUax2g)

* Lateral move
    * [lateral movement fra trend micro](https://blog.trendmicro.com/cyberattack-lateral-movement-explained/)
    * [Common techniques](https://resources.infosecinstitute.com/category/certifications-training/ethical-hacking/post-exploitation-techniques/lateral-movement-techniques/)
    * [Crowdstrike's take on it](https://www.crowdstrike.com/epp-101/lateral-movement/) - Reklameagtig, men har nogle gode tanker.
    * Lateral move on [MITRE tactics](https://attack.mitre.org/tactics/TA0008/)
    * [CIS 20 CSC (version 8)](https://www.cisecurity.org/controls/):  A lot of the controls are applicable to this:
        * 3.3 Configure Data Access Control Lists
        * 6 access control management
        * 16.8 separate production and non-production systems
        
    * [video om flat vs. segmented networks](https://www.youtube.com/watch?v=Xjo9UG90EOo)

* MITRE att&ck frameowrk
    * for reference: [MITRE homepage](https://www.mitre.org/)
        * they run e.g. [cve.org](https://www.cve.org/)
    * [MITRE att&ck website](https://attack.mitre.org/)
    * [Design ang philosophy](https://www.mitre.org/publications/technical-papers/mitre-attack-design-and-philosophy)
    * The mandatory [getting started](https://attack.mitre.org/resources/getting-started/)
    * a bit by [trellix](https://www.trellix.com/en-us/security-awareness/cybersecurity/what-is-mitre-attack-framework.html)
    * Examples: 
        * [non standard port](https://attack.mitre.org/techniques/T1571/)
        * [network segmentation](https://attack.mitre.org/mitigations/M1030/)
        * [Loss of availability](https://attack.mitre.org/techniques/T0826/)
        * [out-of-band communications](https://attack.mitre.org/mitigations/M0810/)
        * [DNS datasource](https://attack.mitre.org/datasources/DS0038/#Passive%20DNS) - se også referencen

## Lektier til næste gang

1. Kig på listen af angrebsteknikker og udvælg 2-3 teknikker

    * søg evt. inspiration i artikler eller andet på nettet om hvordan et angreb har forløbet

2. Undersøg de tilsvarende mitigation teknikker

    * Forklar hvordan man ville sætte mitigation op i et rigtigt system

3. Undersøg de tilsvarende detection teknikker

    * Forklar hvordan man ville sætte detection op i et rigtigt netværk

