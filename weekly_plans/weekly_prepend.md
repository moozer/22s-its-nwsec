---
title: 'ITS NW security'
subtitle: 'Weekly plans'
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mbni@ucl.dk'
left-header: \today
right-header: 'ITS NW security, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the weekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and program for each week of the second semester project.
