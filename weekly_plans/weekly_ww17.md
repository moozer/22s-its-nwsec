---
Week: 17
Content:  Crowdsec
Material: See links in weekly plan
Initials: MON
---

# Uge 17

Vi får besøg fra [crowdsec](https://crowdsec.net/) idag


## Mål for ugen

Practical and learning goals for the period is as follows

### Praktiske mål

Ingen

### Læringsmål

* Den studerende kan forklare formålet med dynamisk opdaterede firewalls, eg. vha. crowdsec
* Den studerende kan starte virtuelle maskiner i skyen

## Leverancer

Ingen

## Tidsplan

Nedenfor er den foreløbige tidsplan, som vil ændre sig afhængigt af input fra de studerende (m.fl.)

### Mandag

Tidsplanen:

| Tidspunkt | Aktivitet |
| :---: | :--- |
|  8:15 | Lektier: gennemgang af tidligere uger |
|       | Spørgsmål, udeståender og highlights |
| 10:00 | Crowdsec kommer |
| 12:00 | Frokost  |
| 12:45 | Intro til Digital ocean og cloud |
| 13:15 | lektier til næste gang |
| 13:30 | the end |

## Kommentarer

* Online companion til dagen er [her](https://moozer.gitlab.io/course-networking-security/06_internet/)

## Lektier til næste gang

* Spin up en DO cloud server
    * Se øvelsen [her](https://moozer.gitlab.io/course-networking-security/06_internet/exposed_server/)
    * Husk at slukke for den igen.
