---
Week: 08
Content:  Netværkstopologi
Material: See links in weekly plan
Initials: MON
---

# Week 8

Netværk er en vigtig del af netværks- og kommunikations sikkerhed. I denne uge ser vi på hvordan man organiserer sig fysisk og logisk med netværk.

Subnets er en praktisk og sikkerhedsmæssig indeling af netværket.


## Goals of the week(s)

Pratical and learning goals for the period is as follows

### Praktisk mål

* Den studerende har vmware workstation kørende, og har en Kali live

### Læringsmål

* Den studerende kan forklare fordele og ulemper ved forskellige topologier
* Den studerende kan forklare segmentering, og dets relation til sikkerhed og netværksperformance
* Den studerende kender til hvordan netværksenheder bruges


## Leverancer

Ingen


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Mandag

Tidsplanen:

| Time | Activity |
| :---: | :--- |
| 8:15 | Lektier: Hardware specs |
| 8:45 | Lektier: Båndbredde check |
| 9:30 | segmentering og subnets |
| 10:45 | vmware og kali    |
| 11:15 | lektier til næste gang |
| 11:30 | Frokost |


## Kommentarer

* Online companion til dagen er [her](https://moozer.gitlab.io/course-networking-security/02_segmentation/)
* Baggrundsmateriale om IP adresser kan finder [her](https://moozer.gitlab.io/course-networking-basisc/03_IP/)


## Lektier til næste gang

* [installer vmware](https://moozer.gitlab.io/course-networking-security/Bonus/install_vmware/)
* [installer kali live](https://moozer.gitlab.io/course-networking-security/Bonus/kali_on_vmware/)
* [sæt en router op](https://moozer.gitlab.io/course-networking-basisc/05_more_routing/setup/#importing-router-1h)
    * og brug wireshark/tcpdump til at checke indkomne vs. outgoing ip pakker.
